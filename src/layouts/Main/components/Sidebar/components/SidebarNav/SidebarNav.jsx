import React, { forwardRef, useContext } from 'react';
import { NavLink as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { List, ListItem, Button, colors, Hidden } from '@material-ui/core';

import AuthContext from '../../../../../../context/auth/authContext';

const useStyles = makeStyles((theme) => ({
  root: {},
  item: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0
  },
  button: {
    color: colors.blueGrey[800],
    padding: '10px 8px',
    justifyContent: 'flex-start',
    textTransform: 'none',
    letterSpacing: 0,
    width: '100%',
    fontWeight: theme.typography.fontWeightMedium
  },
  icon: {
    color: theme.palette.icon,
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(1)
  },
  active: {
    color: theme.palette.primary.main,
    fontWeight: theme.typography.fontWeightMedium,
    '& $icon': {
      color: theme.palette.primary.main
    }
  }
}));

const CustomRouterLink = forwardRef((props, ref) => (
  <div ref={ref} style={{ flexGrow: 1 }}>
    <RouterLink {...props} />
  </div>
));

const renderItem = (classes, page, CustomRouterLink, handleSignOut) => {
  return (
    <ListItem
      key={page.key}
      className={classes.item}
      disableGutters>
      {!page.href ? (
        <Button className={classes.button} onClick={handleSignOut}>
          <div key={page.key} className={classes.icon}>
            {page.icon}
          </div>
          {page.title}
        </Button>
      ) : (
        <Button
          activeClassName={classes.active}
          className={classes.button}
          component={CustomRouterLink}
          to={page.href}>
          <div key={page.key} className={classes.icon}>
            {page.icon}
          </div>
          {page.title}
        </Button>
      )}
    </ListItem>
  );
};

const renderListItem = (classes, page, CustomRouterLink, handleSignOut) => {
  return page.title === 'Account' || page.title === 'Logout' ? (
    <Hidden lgUp key={page.key}>
      {renderItem(classes, page, CustomRouterLink, handleSignOut)}
    </Hidden>
  ) : (
    renderItem(classes, page, CustomRouterLink)
  );
};

const SidebarNav = (props) => {
  const { pages, className, history, ...rest } = props;

  const classes = useStyles();

  // Alert Context
  const authContext = useContext(AuthContext);
  const { singOutUser } = authContext;

  const handleSignOut = () => {
    singOutUser();
    history.push('/sign-in');
  };

  return (
    <List {...rest} className={clsx(classes.root, className)}>
      {pages.map((page) =>
        renderListItem(classes, page, CustomRouterLink, handleSignOut)
      )}
    </List>
  );
};

SidebarNav.propTypes = {
  className: PropTypes.string,
  pages: PropTypes.array.isRequired
};

export default SidebarNav;
