import React, { useContext, useEffect, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Typography } from '@material-ui/core';

import AuthContext from '../../../../../../context/auth/authContext';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content',
    textAlign: 'center',
  },
  avatar: {
    width: 60,
    height: 60
  },
  name: {
    marginTop: theme.spacing(1)
  }
}));

const Profile = (props) => {
  const { className, ...rest } = props;

  const classes = useStyles();

  // Local state
  const [profileUser, setProfileUser] = useState({
    name: null,
    avatar: null,
  });

  const authContext = useContext(AuthContext);
  const { loggedUserInfo } = authContext;

  useEffect(() => {
    if (loggedUserInfo) {
      setProfileUser({
        ...profileUser,
        name: `${loggedUserInfo.first_name} ${loggedUserInfo.last_name}`,
        avatar: loggedUserInfo.gravatar,
      });
    }
  }, [loggedUserInfo]);

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Avatar
        alt="Person"
        className={classes.avatar}
        component={RouterLink}
        src={profileUser.avatar}
        to="/account"
      />

      <Typography className={classes.name} variant="h4">
        {profileUser.name}
      </Typography>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
