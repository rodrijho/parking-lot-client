import React, { Fragment, useState, useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useMediaQuery, Box } from '@material-ui/core';
import { Loading, Notification } from '../../components';

// Context
import AuthContext from '../../context/auth/authContext';
import AlertContext from '../../context/alerts/alertContext';

import { Sidebar, Topbar, Footer } from './components';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: 56,
    height: '100%',
    [theme.breakpoints.up('sm')]: {
      paddingTop: 64
    },
  },
  shiftContent: {
    paddingLeft: 240
  },
  content: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
  }
}));

const Main = (props) => {
  const { children } = props;

  const classes = useStyles();
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
    defaultMatches: true
  });

  // Context
  const authContext = useContext(AuthContext);
  const { isLoggedIn, getLoggedUser, loading, loggedUserInfo } = authContext;

  // Alert Context
  const alertContext = useContext(AlertContext);
  const { alert } = alertContext;

  useEffect(() => {
    if (!alert && !loggedUserInfo) {
      getLoggedUser();
    }
  }, [alert]);

  const [openSidebar, setOpenSidebar] = useState(false);

  const handleSidebarOpen = () => {
    setOpenSidebar(true);
  };

  const handleSidebarClose = () => {
    setOpenSidebar(false);
  };

  const shouldOpenSidebar = isDesktop ? true : openSidebar;

  return (
    <div
      className={clsx({
        [classes.root]: true,
        [classes.shiftContent]: isDesktop
      })}>
      <Topbar onSidebarOpen={handleSidebarOpen} history={props.history} />
      <Sidebar
        onClose={handleSidebarClose}
        open={shouldOpenSidebar}
        variant={isDesktop ? 'persistent' : 'temporary'}
        history={props.history}
      />
      <main className={classes.content}>
        {loading ? (
          <Loading />
        ) : isLoggedIn ? (
          <Fragment>
            {alert ? (
              <Box m={2}>
                <Notification {...alert} />
              </Box>
            ) : null}
            {children}
            <Footer />
          </Fragment>
        ) : (
          <Redirect to="/sign-in" />
        )}
      </main>
    </div>
  );
};

Main.propTypes = {
  children: PropTypes.node
};

export default Main;
