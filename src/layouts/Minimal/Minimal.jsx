import React, { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Loading } from '../../components';

// Context
import AuthContext from '../../context/auth/authContext';

import { Topbar } from './components';

const useStyles = makeStyles(() => ({
  root: {
    paddingTop: 64,
    height: '100%'
  },
  content: {
    height: '100%'
  }
}));

const Minimal = (props) => {
  const { children } = props;

  const classes = useStyles();

  // Context
  const authContext = useContext(AuthContext);
  const { isLoggedIn, getLoggedUser, loading } = authContext;

  useEffect(() => {
      getLoggedUser();
  }, []);

  return (
    <div className={classes.root}>
      <Topbar />
      {loading ? (
        <Loading />
      ) : isLoggedIn ? (
        <Redirect to="/dashboard" />
      ) : (
        <main className={classes.content}>{children}</main>
      )}
    </div>
  );
};

Minimal.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string
};

export default Minimal;
