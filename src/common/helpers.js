const formatResponse = (type, serverMessage) => {
  if (typeof serverMessage === 'undefined' || typeof serverMessage.response === 'undefined') {
    return 'Something went wrong with your request, please try again later.';
  }

  let messageToReturn = '';

  if (type === 'error') {
    const errorResponse = serverMessage.response;
    if (errorResponse.status === 422) {
      messageToReturn =
        typeof errorResponse.data.errors !== 'undefined'
          ? errorResponse.data.errors
          : 'The given data was invalid, please try again.';
    } else {
      messageToReturn =
        typeof errorResponse.data.message !== 'undefined'
          ? errorResponse.data.message
          : 'Something went wrong with your request, please try again later.';
    }
  }

  return messageToReturn;
};

export { formatResponse };
