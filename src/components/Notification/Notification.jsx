import React, { Fragment } from "react";
import PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2)
    }
  },
  list: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  ul: {
    listStyle: 'none'
  }
}));

function renderRow(messages, classes) {
  let index = 0;
  return Array.isArray(messages) === true ? (
    <Fragment>
      <ul className={classes.ul}>
        {messages.map(msg => {
          index++;
          return (
            <li key={index}>
              {msg}
            </li>
          );
        })}
      </ul>
    </Fragment>
  ) : (
    messages
  );
}

const Notification = ({ message, category, duration = 6000 }) => {
  const classes = useStyles();

  return (
    <div className={classes.root} m={2}>
      <Snackbar
        open={true}
        autoHideDuration={duration}
      >
        <Alert severity={`${category}`}>{renderRow(message, classes)}</Alert>
      </Snackbar>
    </div>
  );
};

Notification.propTypes = {
  message: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
  ]).isRequired,
  category: PropTypes.string.isRequired,
  duration: PropTypes.number,
};

export default Notification;
