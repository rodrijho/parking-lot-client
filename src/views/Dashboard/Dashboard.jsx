import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { ParkingQueue, ParkingLot, ParkingSpaces } from './components';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const Dashboard = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={4}>
        <Grid item lg={4} sm={4} xl={4} xs={12}>
          <ParkingLot />
        </Grid>
        <Grid item lg={4} sm={4} xl={4} xs={12}>
          <ParkingQueue />
        </Grid>
        <Grid item lg={4} sm={4} xl={4} xs={12}>
          <ParkingSpaces />
        </Grid>
      </Grid>
    </div>
  );
};

export default Dashboard;
