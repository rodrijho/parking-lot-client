import React, { useEffect, useContext, useState, useCallback } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField,
  CircularProgress,
  colors
} from '@material-ui/core';

import AuthContext from '../../../../context/auth/authContext';
import AlertContext from '../../../../context/alerts/alertContext';

import validate from 'validate.js';

const useStyles = makeStyles((theme) => ({
  root: {},
  buttonProgress: {
    color: colors.green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  textField: {
    marginTop: theme.spacing(2),
    marginLeft: '0',
    marginRight: '0'
  }
}));

const schema = {
  first_name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  last_name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 64
    }
  },
  phone: {
    presence: { allowEmpty: true },
    length: {
      minimum: 0,
      maximum: 16
    }
  }
};

const AccountDetails = (props) => {
  const { user, className, ...rest } = props;

  const classes = useStyles();

  const [isLoading, setLoading] = useState(false);

  // Alert Context
  const alertContext = useContext(AlertContext);
  const { showAlert } = alertContext;

  // Auth Context
  const authContext = useContext(AuthContext);
  const { updateProfile, message, clearMessages, loggedUserInfo } = authContext;

  // Local states
  const [formState, setFormState] = useState({
    isValid: false,
    values: {
      ...user
    },
    touched: {},
    errors: {}
  });

  const handleDetailCallback = useCallback(() => {
    if (message) {
      setLoading(false);
      showAlert(message.message, message.category);
      clearMessages();
    }
  }, [message]);

  useEffect(() => {
    handleDetailCallback();

    const errors = validate(formState.values, schema);

    setFormState((formState) => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values, handleDetailCallback]);

  useEffect(() => {
    setFormState((formState) => ({
      ...formState,
      values: {
        ...formState.values,
        ...user
      }
    }));
  }, [user]);

  // Functions
  const handleChange = (event) => {
    event.persist();

    setFormState((formState) => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]: event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    setLoading(true);

    // Actions
    updateProfile({
      ...formState.values
    });
  };

  const hasError = (field) =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <form autoComplete="off" noValidate onSubmit={handleFormSubmit}>
        <CardHeader subheader="Public information" title="Profile" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                helperText={
                  hasError('first_name') ? formState.errors.first_name[0] : null
                }
                error={hasError('first_name')}
                className={classes.textField}
                label="First name"
                margin="dense"
                name="first_name"
                onChange={handleChange}
                required
                value={formState.values.first_name}
                variant="outlined"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                helperText={
                  hasError('last_name') ? formState.errors.last_name[0] : null
                }
                error={hasError('last_name')}
                className={classes.textField}
                label="Last name"
                margin="dense"
                name="last_name"
                onChange={handleChange}
                required
                value={formState.values.last_name}
                variant="outlined"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                helperText={
                  hasError('email') ? formState.errors.email[0] : null
                }
                error={hasError('email')}
                className={classes.textField}
                label="Email Address"
                margin="dense"
                name="email"
                onChange={handleChange}
                required
                value={formState.values.email}
                variant="outlined"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                helperText={
                  hasError('phone') ? formState.errors.phone[0] : null
                }
                error={hasError('phone')}
                className={classes.textField}
                label="Phone Number"
                margin="dense"
                name="phone"
                onChange={handleChange}
                type="number"
                value={formState.values.phone}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button
            color="primary"
            variant="contained"
            disabled={!formState.isValid || isLoading}
            type="submit">
            {isLoading && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
            Save password
          </Button>
        </CardActions>
      </form>
    </Card>
  );
};

AccountDetails.propTypes = {
  className: PropTypes.string
};

export default AccountDetails;
