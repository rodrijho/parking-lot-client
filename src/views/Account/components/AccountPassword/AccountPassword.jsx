import React, { useContext, useEffect, useState, useCallback } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Grid,
  Button,
  TextField,
  CircularProgress,
  colors
} from '@material-ui/core';

import AuthContext from '../../../../context/auth/authContext';
import AlertContext from '../../../../context/alerts/alertContext';
import validate from 'validate.js';

const useStyles = makeStyles((theme) => ({
  root: {},
  buttonProgress: {
    color: colors.green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  textField: {
    marginTop: theme.spacing(2),
    marginLeft: '0',
    marginRight: '0'
  }
}));

const schema = {
  old_password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 8,
      maximum: 16
    }
  },
  new_password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 8,
      maximum: 16
    }
  },
  new_password_confirmation: {
    presence: { allowEmpty: true },
    equality: 'new_password',
    length: {
      minimum: 8,
      maximum: 16
    }
  }
};

const AccountPassword = (props) => {
  const { history, className, ...rest } = props;

  const classes = useStyles();

  const [isLoading, setLoading] = useState(false);
  const [fromAccountPassword, setFromAccountPassword] = useState(false);

  // Alert Context
  const alertContext = useContext(AlertContext);
  const { showAlert } = alertContext;

  // Auth Context
  const authContext = useContext(AuthContext);
  const { changePassword, message, clearMessages, singOutUser } = authContext;

  // Local states
  const [formState, setFormState] = useState({
    isValid: false,
    values: {
      old_password: '',
      new_password: '',
      new_password_confirmation: ''
    },
    touched: {},
    errors: {}
  });

  const handlePasswordCallback = useCallback(async () => {
    if (message) {
      if (message.category === 'success') {
        if (fromAccountPassword === true) {
          setFormState((formState) => ({
            ...formState,
            values: {
              ...formState.values,
              old_password: '',
              new_password: '',
              new_password_confirmation: ''
            },
            isValid: false,
            touched: {},
            errors: {}
          }));
          singOutUser();
          history.push('/sign-in');
        }
      }

      setLoading(false);
      setFromAccountPassword(false);
      showAlert(message.message, message.category);
      clearMessages();
    }
  }, [message]);

  useEffect(() => {
    handlePasswordCallback();

    const errors = validate(formState.values, schema);

    setFormState((formState) => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values, handlePasswordCallback]);

  // Functions
  const handleChange = (event) => {
    event.persist();

    setFormState((formState) => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]: event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    setLoading(true);
    setFromAccountPassword(true);

    // Actions
    changePassword({
      ...formState.values
    });
  };

  const hasError = (field) =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <form autoComplete="off" noValidate onSubmit={handleFormSubmit}>
        <CardHeader subheader="Account Credentials" title="Profile Password" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item md={12} xs={12}>
              <TextField
                fullWidth
                helperText={
                  hasError('old_password')
                    ? formState.errors.old_password[0]
                    : null
                }
                error={hasError('old_password')}
                className={classes.textField}
                label="Old Password"
                margin="dense"
                name="old_password"
                onChange={handleChange}
                required
                type="password"
                value={formState.values.old_password}
                variant="outlined"
                autoComplete="off"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                helperText={
                  hasError('new_password')
                    ? formState.errors.new_password[0]
                    : null
                }
                error={hasError('new_password')}
                className={classes.textField}
                label="New Password"
                margin="dense"
                name="new_password"
                onChange={handleChange}
                required
                type="password"
                value={formState.values.new_password}
                variant="outlined"
                autoComplete="off"
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                helperText={
                  hasError('new_password_confirmation')
                    ? formState.errors.new_password_confirmation[0]
                    : null
                }
                error={hasError('new_password_confirmation')}
                className={classes.textField}
                label="New Password Confirmation"
                margin="dense"
                name="new_password_confirmation"
                onChange={handleChange}
                required
                type="password"
                value={formState.values.new_password_confirmation}
                variant="outlined"
                autoComplete="off"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button
            color="primary"
            variant="contained"
            disabled={!formState.isValid || isLoading}
            type="submit">
            Save password
            {isLoading && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
          </Button>
        </CardActions>
      </form>
    </Card>
  );
};

AccountPassword.propTypes = {
  className: PropTypes.string
};

export default AccountPassword;
