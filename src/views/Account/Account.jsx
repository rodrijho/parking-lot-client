import React, { useContext, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { AccountDetails, AccountPassword } from './components';
import AuthContext from '../../context/auth/authContext';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const Account = (props) => {
  const {history} = props;

  const classes = useStyles();

  const [values, setValues] = useState({
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    avatar: '',
    old_password: '',
    timezone: 'America/Bogota',
  });

  // Auth Context
  const authContext = useContext(AuthContext);
  const { loggedUserInfo } = authContext;

  useEffect(() => {
    if (loggedUserInfo) {
      setValues({
        ...values,
        first_name: loggedUserInfo.first_name,
        last_name: loggedUserInfo.last_name,
        email: loggedUserInfo.email || '',
        phone: loggedUserInfo.phone_number || '',
        avatar: loggedUserInfo.gravatar
      });
    }
  }, [loggedUserInfo]);

  return (
    <div className={classes.root}>
      <Grid container spacing={4}>
        <Grid item lg={6} md={6} xl={6} xs={12}>
          <AccountDetails user={values} />
        </Grid>
        <Grid item lg={6} md={6} xl={6} xs={12}>
          <AccountPassword user={values} history={history} />
        </Grid>
      </Grid>
    </div>
  );
};

export default Account;
