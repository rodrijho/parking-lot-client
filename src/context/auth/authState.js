import React, { useReducer } from 'react';
import authContext from './authContext';
import authReducer from './authReducer';
import axiosClient from '../../config/axios';
import { formatResponse } from '../../common/helpers';

import {
  REGISTER_SUCCESS,
  LOGIN_SUCCESS,
  REGISTER_ERROR,
  LOGIN_ERROR,
  LOGOUT,
  CLEAR_MESSAGES,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR
} from '../../types';

const AuthState = (props) => {
  const initialState = {
    token: localStorage.getItem('token'),
    isLoggedIn: null,
    loggedUserInfo: null,
    message: null,
    loading: true
  };

  // Dispatch Actions
  const [state, dispatch] = useReducer(authReducer, initialState);

  // Functions

  /**
   * Store a new resource in database
   *
   * @param {*} data
   */
  const signUpUser = async (data) => {
    try {
      const response = await axiosClient.post('access/auth/register', data);
      const formattedResponse = response.data;
      if (formattedResponse.success) {
        dispatch({
          type: REGISTER_SUCCESS,
          payload: {
            data: formattedResponse.data,
            category: 'success'
          }
        });
      } else {
        dispatch({
          type: REGISTER_ERROR,
          payload: {
            message: formattedResponse.message,
            category: 'error'
          }
        });
      }
    } catch (error) {
      const serverMessage = formatResponse('error', error);

      dispatch({
        type: REGISTER_ERROR,
        payload: {
          message: serverMessage,
          category: 'error'
        }
      });
    }
  };

  /**
   * Sign in a user displaying the message according the server response
   *
   * @param {*} data
   */
  const signInUser = async (data) => {
    try {
      const response = await axiosClient.post('access/auth/login', data);
      const formattedResponse = response.data;

      if (formattedResponse.success) {
        dispatch({
          type: LOGIN_SUCCESS,
          payload: {
            data: formattedResponse.data,
            category: 'success'
          }
        });
      } else {
        dispatch({
          type: LOGIN_ERROR,
          payload: {
            message: formattedResponse.message,
            category: 'error'
          }
        });
      }
    } catch (error) {
      const serverMessage = formatResponse('error', error);

      dispatch({
        type: LOGIN_ERROR,
        payload: {
          message: serverMessage,
          category: 'error'
        }
      });
    }
  };

  /**
   * Get the logged user to check if it's authenticated or not
   *
   * @param {*} data
   */
  const getLoggedUser = async (data) => {
    try {
      const token = localStorage.getItem('token');
      if (token) {
        axiosClient.defaults.headers.common[
          'Authorization'
        ] = `bearer ${token}`;
      } else {
        delete axiosClient.defaults.headers.common['Authorization'];

        dispatch({
          type: LOGIN_ERROR,
          payload: {
            message: 'Unauthorized',
            category: 'error'
          }
        });
      }

      const response = await axiosClient.post(
        'access/auth/get-logged-user',
        data
      );
      const formattedResponse = response.data;
      const customData = {
        token: localStorage.getItem('token'),
        user: formattedResponse.data
      };

      if (formattedResponse.success) {
        dispatch({
          type: LOGIN_SUCCESS,
          payload: {
            data: customData,
            category: 'success'
          }
        });
      }
    } catch (error) {
      const serverMessage = formatResponse('error', error);

      dispatch({
        type: LOGIN_ERROR,
        payload: {
          message: serverMessage,
          category: 'error'
        }
      });
    }
  };

  /**
   * Update the profile of the logged user
   *
   * @param {*} data
   */
  const updateProfile = async (data) => {
    try {
      const token = localStorage.getItem('token');
      if (token) {
        axiosClient.defaults.headers.common[
          'Authorization'
        ] = `bearer ${token}`;
      } else {
        delete axiosClient.defaults.headers.common['Authorization'];

        dispatch({
          type: CHANGE_PASSWORD_ERROR,
          payload: {
            message: 'Please login again',
            category: 'error'
          }
        });
      }

      const response = await axiosClient.put(
        `access/profile/info/update`,
        data
      );
      const formattedResponse = response.data;

      dispatch({
        type:
          formattedResponse.success === true
            ? CHANGE_PASSWORD_SUCCESS
            : CHANGE_PASSWORD_ERROR,
        payload: {
          message: formattedResponse.message,
          category: formattedResponse.success === true ? 'success' : 'error',
          data
        }
      });
    } catch (error) {
      const serverMessage = formatResponse('error', error);

      dispatch({
        type: CHANGE_PASSWORD_ERROR,
        payload: {
          message: serverMessage,
          category: 'error'
        }
      });
    }
  };

  /**
   * Change the user credential
   *
   * @param {*} data
   */
  const changePassword = async (data) => {
    try {
      const token = localStorage.getItem('token');
      if (token) {
        axiosClient.defaults.headers.common[
          'Authorization'
        ] = `bearer ${token}`;
      } else {
        delete axiosClient.defaults.headers.common['Authorization'];

        dispatch({
          type: CHANGE_PASSWORD_ERROR,
          payload: {
            message: 'Please login again',
            category: 'error'
          }
        });
      }

      const response = await axiosClient.put(
        `access/profile/change-password`,
        data
      );
      const formattedResponse = response.data;

      dispatch({
        type:
          formattedResponse.success === true
            ? CHANGE_PASSWORD_SUCCESS
            : CHANGE_PASSWORD_ERROR,
        payload: {
          message: formattedResponse.message,
          category: formattedResponse.success === true ? 'success' : 'error'
        }
      });
    } catch (error) {
      const serverMessage = formatResponse('error', error);

      dispatch({
        type: CHANGE_PASSWORD_ERROR,
        payload: {
          message: serverMessage,
          category: 'error'
        }
      });
    }
  };

  /**
   * Logout the user and destroy the token
   */
  const singOutUser = () => {
    dispatch({
      type: LOGOUT,
      message: 'User logged out successfully'
    });
  };

  /**
   * Clear the message from the state to avoid issues displaying the notification
   */
  const clearMessages = () => {
    dispatch({
      type: CLEAR_MESSAGES
    });
  };

  return (
    <authContext.Provider
      value={{
        token: state.token,
        isLoggedIn: state.isLoggedIn,
        loggedUserInfo: state.loggedUserInfo,
        message: state.message,
        loading: state.loading,
        signUpUser,
        signInUser,
        singOutUser,
        getLoggedUser,
        changePassword,
        updateProfile,
        clearMessages
      }}>
      {props.children}
    </authContext.Provider>
  );
};

export default AuthState;
