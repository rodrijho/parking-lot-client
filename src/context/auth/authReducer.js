import {
  REGISTER_SUCCESS,
  LOGIN_SUCCESS,
  REGISTER_ERROR,
  LOGIN_ERROR,
  LOGOUT,
  CLEAR_MESSAGES,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR
} from '../../types';

export default (state, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
      const data = action.payload.data;
      localStorage.setItem('token', data.token);
      const userState = {
        ...state,
        loggedUserInfo: { ...data.user },
        isLoggedIn: true,
        message: null,
        loading: false
      };
      return userState;

    case CHANGE_PASSWORD_SUCCESS:
    case CHANGE_PASSWORD_ERROR:
      const updatedLoggedUser =
        typeof action.payload.data === 'undefined'
          ? { ...state.loggedUserInfo }
          : {
              ...action.payload.data,
              gravatar: action.payload.data.avatar,
              phone_number: action.payload.data.phone
            };

      return {
        ...state,
        loading: false,
        message: action.payload,
        loggedUserInfo: updatedLoggedUser
      };

    case LOGIN_ERROR:
    case REGISTER_ERROR:
    case LOGOUT:
      localStorage.removeItem('token');
      return {
        ...state,
        token: null,
        isLoggedIn: null,
        loggedUserInfo: null,
        message: action.payload,
        loading: false
      };

    case CLEAR_MESSAGES:
      return {
        ...state,
        message: null
      };

    default:
      return state;
  }
};
