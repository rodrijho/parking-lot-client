import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';

// Context
import AuthState from './context/auth/authState';
import AlertState from './context/alerts/alertState';
import tokenAuth from './config/tokenAuth';

// App theme and validation
import { ThemeProvider } from '@material-ui/styles';
import validate from 'validate.js';
import theme from './theme';
import './assets/scss/index.scss';
import validators from './common/validators';

// Application Routes
import Routes from './Routes';

const browserHistory = createBrowserHistory();

// Set token global
const token = localStorage.getItem('token');
if (token) {
  tokenAuth(token);
}

validate.validators = {
  ...validate.validators,
  ...validators
};

export default class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <AlertState>
          <AuthState>
            <Router history={browserHistory}>
              <Routes />
            </Router>
          </AuthState>
        </AlertState>
      </ThemeProvider>
    );
  }
}
